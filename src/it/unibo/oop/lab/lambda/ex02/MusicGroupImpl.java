package it.unibo.oop.lab.lambda.ex02;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Stream;

/**
 *
 */
public class MusicGroupImpl implements MusicGroup {

    private final Map<String, Integer> albums = new HashMap<>();
    private final Set<Song> songs = new HashSet<>();

    @Override
    public void addAlbum(final String albumName, final int year) {
        this.albums.put(Objects.requireNonNull(albumName), year);
    }

    @Override
    public void addSong(final String songName, final Optional<String> albumName, final double duration) {
        if (albumName.isPresent() && !this.albums.containsKey(albumName.get())) {
            throw new IllegalArgumentException("invalid album name");
        }
        this.songs.add(new MusicGroupImpl.Song(Objects.requireNonNull(songName), Objects.requireNonNull(albumName), duration));
    }

    @Override
    public Stream<String> orderedSongNames() {
        return songs.stream()
                    .map(Song::getSongName)
                    .sorted();
    }

    @Override
    public Stream<String> albumNames() {
        return albums.keySet().stream();
    }

    @Override
    public Stream<String> albumInYear(final int year) {
        return albums.entrySet().stream()
                                .filter(entry -> entry.getValue() == year)
                                .map(entry -> entry.getKey());
    }

    @Override
    public int countSongs(final String albumName) {
        return (int) songsInAlbum(albumName).count();
    }

    @Override
    public int countSongsInNoAlbum() {
        return (int) songs.stream()
                    .filter(song -> !song.getAlbumName().isPresent())
                    .count();
    }

    @Override
    public OptionalDouble averageDurationOfSongs(final String albumName) {
        return songsInAlbum(albumName).mapToDouble(Song::getDuration)
                                      .average();
    }

    @Override
    public Optional<String> longestSong() {
        final Optional<Song> longestSong = songs.stream()
                                                .max((song1, song2) -> Double.compare(song1.getDuration(), song2.getDuration()));
        return longestSong.isPresent() ? Optional.of(longestSong.get().getSongName()) : Optional.empty();
    }

    @Override
    public Optional<String> longestAlbum() {
        final Optional<AbstractMap.SimpleEntry<String, Double>> longestAlbum = albums.entrySet().stream()
                                                    .map(entry -> new AbstractMap.SimpleEntry<>(entry.getKey(), albumDuration(entry.getKey())))
                                                    .max((album1, album2) -> Double.compare(album1.getValue(), album2.getValue()));
        return longestAlbum.isPresent() ? Optional.of(longestAlbum.get().getKey()) : Optional.empty();
    }

    private Stream<Song> songsInAlbum(final String albumName) {
        return songs.stream()
                    .filter(song -> song.getAlbumName().isPresent())
                    .filter(song -> song.getAlbumName().get().equals(albumName));
    }

    private double albumDuration(final String albumName) {
        return songsInAlbum(albumName).mapToDouble(Song::getDuration)
                                      .sum();
    }

    private static final class Song {

        private final String songName;
        private final Optional<String> albumName;
        private final double duration;
        private int hash;

        Song(final String name, final Optional<String> album, final double len) {
            super();
            this.songName = name;
            this.albumName = album;
            this.duration = len;
        }

        public String getSongName() {
            return songName;
        }

        public Optional<String> getAlbumName() {
            return albumName;
        }

        public double getDuration() {
            return duration;
        }

        @Override
        public int hashCode() {
            if (hash == 0) {
                hash = songName.hashCode() ^ albumName.hashCode() ^ Double.hashCode(duration);
            }
            return hash;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof Song) {
                final Song other = (Song) obj;
                return albumName.equals(other.albumName) && songName.equals(other.songName)
                        && duration == other.duration;
            }
            return false;
        }

        @Override
        public String toString() {
            return "Song [songName=" + songName + ", albumName=" + albumName + ", duration=" + duration + "]";
        }

    }

}
